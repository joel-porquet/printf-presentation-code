#include <printf.h>
#include <stdio.h>

struct complex {
	int real, img;
};

int print_complex(FILE *fp, const struct printf_info *info,
				  const void * const *args)
{
	int len;
	struct complex *c = *((struct complex**)(args[0]));

	len = fprintf(fp, "(%d%+di)", c->real, c->img);

	return len;
}

int print_complex_arginfo (const struct printf_info *info, size_t n,
						   int *argtypes)
{
	/* We always take exactly one argument and this is a pointer to the
	   structure.. */
	if (n > 0)
		argtypes[0] = PA_POINTER;
	return 1;
}

int main(void)
{
	struct complex c = {
		.real = 7,
		.img = 3
	};

	register_printf_function('C', print_complex, print_complex_arginfo);

	printf("My complex number is %C!\n", &c);
}
