#include <stdarg.h>
#include <stdio.h>

void my_printf(char *fmt, ...)
{
	/* ??? */
}

int main(void)
{
    /* printf 101 */
    my_printf("Hello world!\n");

	/* printf 201: basic placeholders */
	my_printf("%s from %c to Z, in %d minutes!\n",
			  "printf", 'A', 0);

	return 0;
}
