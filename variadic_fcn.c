#include <stdarg.h>	/* Macro/function definitions for variadic functions */
#include <stdio.h>

/*
 * Sum a variable number of integers
 * @count: the number of integer parameters
 *
 * Receive @count integers, sum them up and return the result
 */
int sum_ints(int count, ...)
{
	va_list ap;
	int i, sum = 0;

	va_start(ap, count);				/* Init variable parameter list */

	for (i = 0; i < count; i++) {
		int n = va_arg(ap, int);		/* Get next integer parameter */
		sum += n;
	}

	va_end(ap);							/* Clean up parameter list */

	return sum;
}

int main(void)
{
	int res;

	/* sum up 3 integers: 10, 20 and 30 */
	printf("Sum is %d\n", sum_ints(3, 10, 20, 30));

	/* sum up 5 integers: 10, 20, 30, 40, 50 */
	printf("Sum is %d\n", sum_ints(5, 10, 20, 30, 40, 50));

	return 0;
}
