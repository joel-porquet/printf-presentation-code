#include <stdio.h>

int main(void)
{
	/* printf 101 */
	printf("Hello world!\n");

	/* printf 201 */
	printf("%s from %c to Z, in %d minutes!\n", "printf", 'A', 45);

	/* printf 10^101 */
	int i;
	printf("\b%n", &i);
	printf("%s\bD is \033[1;31m#%d\033[0m!\n", "UCB", i);
}
