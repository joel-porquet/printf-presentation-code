src := $(sort $(wildcard *.c))
bin := $(src:%.c=%)

all: $(bin)

clean:
	-rm -f $(bin)
