#include <stdarg.h>
#include <stdio.h>

void my_printf(char *fmt, ...)
{
	va_list ap;
	char c;
	int d;
	char *s;

	va_start(ap, fmt);

    while (*fmt) {
		/* Check if character is '%' */
		if (*fmt != '%') {
			/* If not, display without no processing and
			 * continue to the next character
			 */
			putchar(*fmt++);
			continue;
		}

		/* Skip '%' and get to the placeholder */
		fmt++;
		/* Distinguish different placeholders */
		switch(*fmt) {
			case 'c':
				/* Get character from arguments */
				c = va_arg(ap, int);
				putchar(c);
				break;
			case 's':
				/* Get string pointer from arguments */
				s = va_arg(ap, char*);
				/* Display each char from the string */
				while (*s) putchar(*s++);
				break;
			case 'd':
				/* Get integer from arguments */
				d = va_arg(ap, int);
				/* Translate integer into string
				 * (via ASCII conversion) */
				char buf[10], *end = buf;
				do {
					*end = (d % 10) + '0';
					end++;
					d /= 10;
				} while (d);
				/* Display string */
				while (end != buf) {
					putchar(*--end);
				}
				break;
		}
		/* Skip placeholder and continue */
		fmt++;
	}

	va_end(ap);
}

int main(void)
{
    /* printf 101 */
    my_printf("Hello world!\n");

	/* printf 201: basic placeholders */
	my_printf("%s from %c to Z, in %d minutes!\n",
			  "printf", 'A', 45);

	return 0;
}
