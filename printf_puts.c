#include <stdio.h>

void my_printf(char *fmt)
{
	while (*fmt)
		putchar(*fmt++);
}

int main(void)
{
	/* printf 101 */
	my_printf("Hello world!\n");
}
